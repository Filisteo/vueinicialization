function logged (req,res,next){
    //TENGO VARIABLE DE SESSION TOKEN req.session.authToken; 
    if(req.session.authToken === req.session.userToken){
        next();
    }else{
        res.status(403).send("YOU DON'T HAVE PERMISSIONS");
    }
};

module.exports = {logged};