-- DropForeignKey
ALTER TABLE `file` DROP FOREIGN KEY `File_repoId_fkey`;

-- AddForeignKey
ALTER TABLE `File` ADD CONSTRAINT `File_repoId_fkey` FOREIGN KEY (`repoId`) REFERENCES `Repository`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
