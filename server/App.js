const express = require("express");
const app = express();
const cors = require("cors");
const firebase = require("firebase/app");
const port = 3000;
const logged = require("./helper/onAuthStateChange").logged;
const session = require("express-session");

app.use(cors());

app.use(
  session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
  })
);
app.use(express.urlencoded());
app.use(express.json());

//ROUTES
const routes = require("./Router/Routes.routes");

app.use(logged, routes);

const firebaseConfig = {
  apiKey: "AIzaSyCKqu0QJm7vHdR6B9oXaDChZPLWNNUNyrY",

  authDomain: "vite-app-d060e.firebaseapp.com",

  projectId: "vite-app-d060e",

  storageBucket: "vite-app-d060e.appspot.com",

  messagingSenderId: "909047822583",

  appId: "1:909047822583:web:f66030aa8b322b9792bbf2",

  measurementId: "G-GHP0DB2575",
};

firebase.initializeApp(firebaseConfig);

app.get("/", (req, res) => {
  console.log("Connected");
});

app.listen(port, () => {
  console.log(`Escuchando en el puerto: ${port}`);
});
