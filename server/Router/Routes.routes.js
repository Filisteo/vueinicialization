const express = require('express');
const router = express.Router();
const UserController = require('../Controller/UserController');
const RepoController = require('../Controller/RepoController');
const FileController = require('../Controller/FileController');


//USER CRUD
router.get('/Home/:email', UserController.show);
router.get('/Home', UserController.showAll);
router.post('/Create', UserController.create);
router.post('/Update', UserController.update);
router.post('/Delete', UserController.delete);

//LOGIN
router.post('/LogIn', UserController.logIn);
router.get('/SignOut', UserController.signOut);

//REPOSITORY
router.post('/CreateRepository', RepoController.create);
router.get('/ShowRepository/:id', RepoController.showAll);
router.get('/LastRepo/:id', RepoController.lastRepo);
router.post('/DeleteRepository', RepoController.delete);
router.post('/UpdateRepository', RepoController.update);

//FILES
router.post('/CreateFile', FileController.create);
router.get('/ShowFile', FileController.showAll);
router.get('/ShowFileOne/:id',FileController.show);
router.post('/DeleteFile', FileController.delete);
router.post('/UpdateFile', FileController.update);


module.exports = router;