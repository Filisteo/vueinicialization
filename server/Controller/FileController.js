const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const controller = {};

//CRUD

//CREAD
controller.create = (req,res) => {
    req.session.userToken = req.body.headers.Authorization;
    let title = req.body.title.trim();
    let description = req.body.description.trim();
    let extension = req.body.extension.trim();
    let repoId = parseInt(req.body.id);
    async function main() {
      const file = await prisma.file.create({
        data: {
          title: title,
          description: description,
          extension: extension,
          repoId: repoId,
        },
      });
      res.status(200).json(file);
    }
  
    main()
      .catch((error) => {
        throw error;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  };

  //READ ALL
controller.showAll = (req, res) => {
  req.session.userToken = req.headers.authorization;
    async function main() {
      const files = await prisma.file.findMany();
      res.status(200).json(files);
    }
  
    main()
      .catch((error) => {
        throw error;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  };

  controller.show = (req,res) => {
    req.session.userToken = req.headers.authorization;
    async function main() {
      const files = await prisma.file.findUnique({
        where: {
          id: parseInt(req.params.id),
        },
      });
      res.status(200).json(files);
    }
  
    main()
      .catch((error) => {
        throw error;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }

  //DELETE
  controller.delete = (req,res) => {
    console.log(req.body)
    req.session.userToken = req.body.headers.Authorization;
    async function main() {
      const remove = await prisma.file.delete({
        where: {
          id: req.body.id
        }
      });
     
      if(remove){
        console.log('File was delete')
        //res.status(200).json('success')
      }
    }
  
    main()
      .catch((error) => {
        throw error;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }

  //UPDATE
  controller.update = (req, res) => {
    req.session.userToken = req.body.headers.Authorization;
    let title = req.body.title.trim();
    let description = req.body.description.trim();
    let extension = req.body.extension.trim();
  async function main() {
    const file = await prisma.file.update({
      where: {
        id: req.body.id,
      },
      data: {
        extension: extension,
        description: description,
        title: title,
      },
    });
    console.log(file);
    res.status(200).json(file);
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
  }

module.exports = controller;