const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const controller = {};

//CRUD

//CREAD
controller.create = (req, res) => {
    let title = req.body.title.trim();
    let authorId = req.body.id;
    req.session.userToken = req.body.headers.Authorization;
    async function main() {
      const repository = await prisma.repository.create({
        data: {
          title: title,
          authorId: authorId,
        },
      });
      console.log(repository);
      res.status(200).json(repository);
    }
  
    main()
      .catch((error) => {
        throw error;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  };

  //READ
controller.showAll = (req, res) => {
    let id = parseInt(req.params.id);
    req.session.userToken = req.headers.authorization;
    async function main() {
      const repository = await prisma.repository.findMany({
        where:{
          authorId: id
        }
      });
      res.status(200).json(repository);
    }
  
    main()
      .catch((error) => {
        throw error;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  };

  //DELETE
  controller.delete = (req) => {
    req.session.userToken = req.body.headers.Authorization;
    async function main() {
      const remove = await prisma.repository.delete({
        where: {
          id: req.body.id
        }
      });
     
      if(remove){
        console.log('Repository success removed')
      }
    }
  
    main()
      .catch((error) => {
        throw error;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }

  //UPDATE
  controller.update = (req, res) => {
    req.session.userToken = req.body.headers.Authorization;
    let title = req.body.title.trim();
  async function main() {
    const repo = await prisma.repository.update({
      where: {
        id: req.body.id,
      },
      data: {
        title: title,
      },
    });
    console.log(repo);
    res.status(200).json(repo);
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
  };

//LASTREPO
controller.lastRepo = (req,res) => {
  req.session.userToken = req.headers.Authorization;
  let authorId = parseInt(req.params.id);
  async function main() {
    const lastOne = await prisma.repository.findMany({
      where: {
        authorId: authorId
      },
      orderBy: {
          createdAt: 'desc',
      },
      take: 1,
    })
   res.status(200).json(lastOne)
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
}



module.exports = controller;