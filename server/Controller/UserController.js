const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const firebase = require("firebase/auth");

const controller = {
  logges: false,
};

//CRUD

//READ
controller.show = (req, res) => {
  let email = req.params.email;
  async function main() {
    const user = await prisma.user.findUnique({
      where: {
        email: email,
      },
    });
    res.status(200).json(user);
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
};

//READ ALL
controller.showAll = (req, res) => {
  async function main() {
    const user = await prisma.user.findMany();
    res.status(200).json(user);
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
};

//CREATE
controller.create = (req, res) => {
  let name = req.body.name.trim();
  let password = req.body.password.trim();
  let email = req.body.email.trim();
  async function main() {
    const user = await prisma.user.create({
      data: {
        name: name,
        password: password,
        email: email,
        image: '',
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
    
    if (user != null) {
      firebase
        .createUserWithEmailAndPassword(firebase.getAuth(), email, password)
        .then((data) => {
          console.log("Succesfully registered");
          const isLogged = true;
          req.session.authToken = data.accessToken;
          req.session.user_id = data.uid;
          console.log(`User '${req.body.name} is: ${isLogged}`);
          console.log(user)
          res.status(200).json({ data, user });
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      res.status(200).json("Username has already exists");
    }
  }

  main()
    .catch((error) => {
      if (error.code == "P2002") {
        res.status(200).json("User already exist");
      }
      console.log(error);
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
};

//UPDATE

controller.update = (req, res) => {
  let name = req.body.name.trim();
  let email = req.body.email.trim();
  let image = req.body.img.trim();
  console.log(image, email, name);
  async function main() {
    const user = await prisma.user.update({
      where: {
        email: email,
      },
      data: {
        name: name,
        image: image
      },
    });
    res.status(200).json(user);
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
};

//DELETE

controller.delete = (req, res) => {
  let email = req.body.email.trim();
  async function main() {
    const user = await prisma.user.delete({
      where: {
        email: email,
      },
      select: {
        email: true,
        password: true,
      },
    });
    if (user != null) {
      firebase
        .deleteUser(firebase.getAuth().currentUser)
        .then(() => {
          console.log("User deleted firebase succesfully");
          res.status(200).json(true);
          delete req.session.authToken;
          delete req.session.user_id;
        })
        .catch((error) => {
          console.log(error);
        });
    }
    console.log("Succes delete User");
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
};

//LOGIN FIREBASE PRISMA
controller.logIn = (req, res) => {
  let email = req.body.email.trim();
  let password = req.body.password.trim();

  async function main() {
    let user = await prisma.user.findUnique({
      where: {
        email: email,
      },
    });
    if (user != null) {
      firebase
        .signInWithEmailAndPassword(firebase.getAuth(), email, password)
        .then((response) => {
          response.user
            .getIdTokenResult(true)
            .then((data) => {
              console.log("User Logged Succesfully");
              res.status(200).json({ data, user });
              req.session.authToken = data.token;
              req.session.user_id = data.claims.user_id;
            })
            .catch((error) => {
              console.log(error);
            });
        })
        .catch((error) => {
          let errMsg = "";
          switch (error.code) {
            case "auth/user-not-found":
              errMsg = "Invalid email";
              res.status(200).json(errMsg);
              break;
            case "auth/wrong-password":
              errMsg = "Incorrect password";
              res.status(200).json(errMsg);
              break;
          }
        });
    } else {
      res.status(200).json("User does not exist");
    }
  }

  main()
    .catch((error) => {
      throw error;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
};

//SIGNOUT
controller.signOut = (req, res) => {
  firebase
    .signOut(firebase.getAuth())
    .then(() => {
      console.log("Succesfully LogOut");
      const isLogged = false;
      console.log(isLogged);
      delete req.session.authToken;
      delete req.session.user_id;
      res.status(200).json(isLogged);
    })
    .catch((error) => {
      console.log(error);
    });
};

module.exports = controller;
