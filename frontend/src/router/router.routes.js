import { createRouter, createWebHistory } from "vue-router";

//COMPONENTS
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Login from "../views/Login.vue";
import SingUp from "../views/SingUp.vue";
import User from "../views/User.vue";
import CreateFile from "../components/Formularios/CreateFile.vue"; 
import File from "../views/File.vue";
import PageNotFound from "../views/PageNotFound.vue";

//DEFINE ROUTES
const routes = [
    {path: '/', component: Home },
    //RUTA PARA PASAR DATOS DEL REGISTRO AL MAIN
    {path: '/Home', name: 'Home', component: Home},
    {path: '/About', component: About},
    {path: '/Login', name: 'Login', component: Login},
    {path: '/Sing Up', component: SingUp}, 
    {path: '/User', component: User},
    {path: '/CreateFile/:id', component: CreateFile},
    {path: '/File', component: File},
    {path: '/:pathMatch(.*)*', name:'not-found', component: PageNotFound}
];


const history = createWebHistory();

const router = createRouter({
    history,
    routes,
});

router.resolve({
    name:'not-found',
    params: { pathMatch: ['not', 'found'] },
}).href

export default router;
