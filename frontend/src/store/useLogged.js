import { defineStore } from 'pinia'

export const loggedStore = defineStore('logged',{
    
    state: () => {
        return {islogged: false}
    },

    actions: {
        changeLogged (loggedStatus){
            this.islogged = loggedStatus;
        }
    }
})
 

