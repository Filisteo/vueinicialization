import { defineStore } from "pinia";

export const User = defineStore("User", {
  state: () => ({
    user: {},
    users: []
  }),

  actions: {
    chargeUser(user) {
        this.user = user;
    },

    chargeUsers(users){
      this.users.push(users);
    },

    orderUsers(users){
      this.users = users;
    },

    orderUsersArray(index,repo){
      this.users.splice(index,1,repo)
    },
    
    reset(){
        this.user = {};
    },
  },
});
