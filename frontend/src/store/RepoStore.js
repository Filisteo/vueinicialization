import { defineStore } from "pinia";

export const Repo = defineStore("Repo", {
  state: () => ({
    repository: {},
    repositories:[],
  }),

  actions: {
    chargeRepo(repo) {
      this.repository = repo;
    },

    chargeRepos(repos){
        this.repositories.push(repos)
    },

    orderRepos(index,repo){
      this.repositories.splice(index,1,repo)
    },

    orderReposEliminated(index){
      this.repositories.splice(index,1)
    },
    
    reset(){
        this.repositories = {};
    },
  },
});