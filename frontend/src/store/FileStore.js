import { defineStore } from "pinia";

export const File = defineStore("File", {
  state: () => ({
    file: {},
    files:[],
  }),

  actions: {
    chargeFile(file) {
        this.file = file;
    },

    chargeFiles(files){
        this.files.push(files)
    },

    orderFiles(index){
      this.files.splice(index,1);
    },
    
    reset(){
        this.files = [];
    },
  },
});