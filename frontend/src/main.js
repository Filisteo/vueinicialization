import { createApp } from "vue";
import App from "./App.vue";
import axios from "axios";
import router from "./router/router.routes";
import { createPinia } from 'pinia';

axios.interceptors.request.use(config => {
  config.headers.authorization = sessionStorage.getItem('token');
  return config;
})


createApp(App)
  .use(createPinia())
  .use(router)
  .mount("#app")
